FROM node:12.14
WORKDIR /app
COPY . .
RUN npm i && npm run build --prod

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/dist/angular-nginx-docker .
CMD ["nginx", "-g", "daemon off;"]